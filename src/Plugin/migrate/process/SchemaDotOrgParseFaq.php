<?php

declare(strict_types=1);

namespace Drupal\schemadotorg_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Parse FAQ from HTML.
 *
 * @MigrateProcessPlugin(
 *   id = "schemadotorg_parse_faq"
 * )
 *
 * @code
 * schema_main_entity:
 *   plugin: transform_value
 *   type: questions|introduction
 *   header: h2
 *   source: body/0/value
 * @endcode
 */
class SchemaDotOrgParseFaq extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // PHP's \DOMDocument::saveXML() encodes carriage returns as &#13; so
    // normalize all newlines to line feeds.
    // @todo Remove once Drupal 10.1.x+ is only supported.
    // @see https://www.drupal.org/project/drupal/issues/3311595
    if (floatval(\Drupal::VERSION) < 10.1) {
      $value = str_replace(["\r\n", "\r"], "\n", $value);
    }

    $type = $this->configuration['type'] ?? NULL;
    $header = $this->configuration['header'] ?? 'h2';
    $dom_node = Html::load($value)->getElementsByTagName('body')->item(0);

    // Set sections.
    $sections = [
      'introduction' => [
        'id' => '',
        'title' => '',
        'dom' => new \DOMDocument(),
      ],
    ];

    $id = 'introduction';
    $index = 0;
    foreach ($dom_node->childNodes as $child_node) {
      $tag_name = $child_node->tagName ?? NULL;
      $text_content = trim($child_node->textContent);
      if ($tag_name === $header && !empty($text_content)) {
        $id = 'header-' . (++$index);
        $sections[$id] = [
          'id' => $id,
          'title' => $text_content,
          'dom' => new \DOMDocument(),
        ];
      }
      else {
        $sections[$id]['dom']->appendChild(
          $sections[$id]['dom']->importNode($child_node, TRUE)
        );
      }
    }

    // Build the FAQs introduction and questions.
    $faq = [
      'introduction' => '',
      'questions' => [],
    ];
    foreach ($sections as $id => $section) {
      /** @var \DOMDocument $dom */
      $dom = $section['dom'];

      $html = '';
      foreach ($dom->childNodes as $node) {
        $html .= $dom->saveXML($node);
      }
      $html = trim($html);

      if ($id === 'introduction') {
        $faq[$id] = $html;
      }
      else {
        $faq['questions'][] = [
          'name' => $section['title'],
          'accepted_answer' => $html,
        ];
      }
    }

    return $faq[$type] ?? $faq;
  }

}
