Table of contents
-----------------

* Introduction
* Features
* Notes
* References
* Drush Commands


Introduction
------------

Experiments with migrating existing content types to use Schema.org types.

Experiments include

- Convert a page to an Schema.org FAQPage.


Features
--------

FAQPage migration

- Create default FAQ page.
- Provides a schemadotorg_parse_faq migrate process plugins.
- Provides convert_page_to_faq migration


Notes
-----

- We need to determine the best way convert/route page with FAQs 
  to the faq content types, while continuing to migrate other pages AS-IS. 

Related Issues

- [Issue #3069776: SQL source plugins: allow defining conditions and join in migration yml](https://www.drupal.org/project/drupal/issues/3069776)
- [Issue #3254390: Allow configurable entity query conditions on the Content Entity source plugin](https://www.drupal.org/project/drupal/issues/3254390)  


References
----------

Articles

- [Migrate Drupal module](https://www.drupal.org/docs/core-modules-and-themes/core-modules/migrate-drupal-module)
- [Migrate API | Drupal APIs](https://www.drupal.org/docs/drupal-apis/migrate-api)
- [Tips for writing Drupal migrations and understanding their workflow](https://understanddrupal.com/lessons/tips-writing-drupal-migrations-and-understanding-their-workflow/)
- [How to migrate from Drupal 8 to Drupal 9 | antrecu](http://antrecu.com/blog/how-migrate-drupal-8-drupal-9)
- [Third-party Content Synchronization with Drupal Migrate](https://atendesigngroup.com/articles/third-party-content-synchronization-drupal-migrate)

Modules

- [Migrate Plus | Drupal.org](https://www.drupal.org/project/migrate_plus)
- [Migrate Tools | Drupal.org](https://www.drupal.org/project/migrate_tools)
- [Drupal 8+ migration (source)](https://www.drupal.org/project/migrate_drupal_d8)  
  _Needed if Drupal 8 is outside the current instance._
- [Migrate Conditions | Drupal.org](https://www.drupal.org/project/migrate_conditions)


Drush Commands 
--------------

```bash
# Collect default content uuids. 
echo 'node:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM node";

# Export default content.
ddev drush default-content:export-module schemadotorg_migrate;
```

```
ddev drush list --filter=migrate

Available commands:                                                              
migrate:                                                                         
migrate:fields-source (mfs) List the fields available for mapping in a source.
migrate:import (mim)        Perform one or more migration processes.           
migrate:messages (mmsg)     View any messages associated with a migration.     
migrate:reset-status (mrs)  Reset an active migration's status to idle.        
migrate:rollback (mr)       Rollback one or more migrations.                   
migrate:status (ms)         List all migrations with current status.           
migrate:stop (mst)          Stop an active migration operation.                
```

```
# List plugins.
ddev drush eval "
  foreach (['source', 'destination', 'process', 'field', 'id_map'] as \$type) {
    print PHP_EOL . strtoupper(\$type) . PHP_EOL;
    print str_repeat('-', strlen(\$type)) . PHP_EOL;
    print implode(PHP_EOL, array_keys(\Drupal::service('plugin.manager.migrate.' . \$type)->getDefinitions())) . PHP_EOL;
  }
";
```

```
# Clear plugin cache.
ddev drush cache-clear plugin

# Clear migrate plugin cache.
ddev drush eval "foreach (['migration', 'migrate.source', 'migrate.destination', 'migrate.process', 'migrate.destination', 'migrate.id_map'] as \$type) \Drupal::service('plugin.manager.' . \$type)->clearCachedDefinitions();";

# Status
ddev drush migrate:status convert_page_to_faq

# Import
ddev drush migrate:import convert_page_to_faq

# Update 
ddev drush migrate:import convert_page_to_faq --update

# Stop
ddev drush migrate:stop convert_page_to_faq
ddev drush migrate:reset-status convert_page_to_faq
```
