<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_migrate\Kernel;

use Drupal\migrate\MigrateExecutable;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\schemadotorg\Kernel\SchemaDotOrgEntityKernelTestBase;
use Drupal\user\Entity\User;

/**
 * Test Schema.org Blueprints in-place migration.
 *
 * @group schemadotorg
 */
class SchemaDotOrgMigrateKernelTest extends SchemaDotOrgEntityKernelTestBase {
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  // phpcs:disable
  /**
   * Disabled config schema checking until the custom field module has a schema.
   */
  protected $strictConfigSchema = FALSE;
  // phpcs:enable

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'default_content',
    'serialization',
    'custom_field',
    'schemadotorg_custom_field',
  ];

  /**
   * {@inheritdoc}
   *
   * Set ups and install the default content.
   *
   * @see \Drupal\Tests\default_content\Kernel\DefaultContentYamlImportTest::setUp
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user 1.
    User::create([
      'uid' => 1,
      'name' => 'user1',
    ])->save();

    // Create 'page' content type.
    $this->installEntitySchema('node');
    $this->installConfig(['node']);
    $this->createContentType(['type' => 'page']);

    // Enable the module and import the content.
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
    $module_installer = $this->container->get('module_installer');
    $module_installer->install(['schemadotorg_migrate']);

    // Create 'faq' Schema.org content type.
    $this->installConfig(['schemadotorg_custom_field']);
    $this->createSchemaEntity('node', 'FAQPage');
  }

  /**
   * Tests in-place migration.
   */
  public function testMigration(): void {
    /** @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_manager */
    $migration_manager = $this->container->get('plugin.manager.migration');
    /** @var \Drupal\node\NodeStorageInterface $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    // Check that the page node exists.
    $nodes = $node_storage->loadByProperties(['uuid' => '8b33162b-ef4b-467a-8f53-af53df20f3e2']);
    $node = reset($nodes);
    $this->assertEquals('Frequently Asked Questions (FAQ) about Drupal', $node->label());

    // Execute the migration.
    // @see \Drupal\Tests\migrate\Kernel\MigrateTestBase::executeMigrations
    // @see \Drupal\Tests\migrate\Kernel\MigrateTestBase::executeMigration
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
    $migration = $migration_manager->createInstance('convert_page_to_faq');
    $migrate = new MigrateExecutable($migration);
    $migrate->import();

    // Get the faq node.
    $nodes = $node_storage->loadByProperties(['type' => 'faq']);
    $node = reset($nodes);

    // Check that the introduction was moved the body.
    $this->assertEquals(
      '<p>These are just a few commonly asked questions about Drupal. Depending on specific use cases and requirements, there can be additional questions and answers related to Drupal development, site administration, performance optimization, and more.</p>',
      $node->body->value
    );

    // Check that the questions were parsed into a custom field.
    $value = $node->schema_faq_main_entity->getValue();
    $this->assertEquals(
      'How does Drupal differ from other content management systems (CMS)?',
      $value[1]['name']
    );
    $this->assertEquals(
      "<p>Drupal differentiates itself from other CMS platforms with its robustness and flexibility. It offers a highly customizable and modular architecture, which allows developers to create complex websites with specific requirements. Drupal also has a strong community and a wide range of contributed modules and themes, making it a popular choice for developers.</p>",
      $value[1]['accepted_answer']
    );
  }

}
