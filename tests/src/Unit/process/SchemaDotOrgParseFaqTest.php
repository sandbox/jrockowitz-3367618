<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_migrate\Unit\process;

use Drupal\schemadotorg_migrate\Plugin\migrate\process\SchemaDotOrgParseFaq;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;

/**
 * Tests the Schema.org parse FAQ process plugin.
 *
 * @group schemadotorg_migrate
 *
 * @coversDefaultClass \Drupal\schemadotorg_migrate\Plugin\migrate\process\SchemaDotOrgParseFaq
 */
class SchemaDotOrgParseFaqTest extends MigrateProcessTestCase {

  /**
   * Tests schemadotorg_parse_faq.
   *
   * @param array $configuration
   *   The plugin's configuration.
   * @param string $source
   *   The source value.
   * @param mixed $expected_result
   *   The expected result.
   *
   * @covers ::transform
   *
   * @dataProvider transformDataProvider
   *
   * @throws \Drupal\migrate\MigrateException
   */
  public function testTransform(array $configuration, string $source, mixed $expected_result): void {
    $plugin = new SchemaDotOrgParseFaq($configuration, 'schemadotorg_parse_faq', []);
    $result = $plugin->transform($source, $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($expected_result, $result);
  }

  /**
   * Provides Data for ::testTransform.
   */
  public function transformDataProvider(): array {
    return [
      // Check empty string.
      [
        'configuration' => [
          'type' => NULL,
          'header' => NULL,
        ],
        'source' => '',
        'expected_result' => ['introduction' => '', 'questions' => []],
      ],
      // Check expected HTML markup.
      [
        'configuration' => [],
        'source' => '<p>Introduction</p><h2>Question 1</h2><p>Answer 1</p><h2>Question 2</h2><p>Answer 2</p>',
        'expected_result' => [
          'introduction' => '<p>Introduction</p>',
          'questions' => [
            [
              'name' => 'Question 1',
              'accepted_answer' => '<p>Answer 1</p>',
            ],
            [
              'name' => 'Question 2',
              'accepted_answer' => '<p>Answer 2</p>',
            ],
          ],
        ],
      ],
      // Check expected HTML markup with spaces.
      [
        'configuration' => [],
        'source' => '  <p>Introduction</p>  <h2>Question 1</h2>  <p>Answer 1</p> <h2> Question 2 </h2> <p>Answer 2</p>  ',
        'expected_result' => [
          'introduction' => '<p>Introduction</p>',
          'questions' => [
            [
              'name' => 'Question 1',
              'accepted_answer' => '<p>Answer 1</p>',
            ],
            [
              'name' => 'Question 2',
              'accepted_answer' => '<p>Answer 2</p>',
            ],
          ],
        ],
      ],
      // Check using H3 as the header.
      [
        'configuration' => ['header' => 'h3'],
        'source' => '<h3>Question 1</h3><p>Answer 1</p><h3>Question 2</h3><p>Answer 2</p>',
        'expected_result' => [
          'introduction' => '',
          'questions' => [
            [
              'name' => 'Question 1',
              'accepted_answer' => '<p>Answer 1</p>',
            ],
            [
              'name' => 'Question 2',
              'accepted_answer' => '<p>Answer 2</p>',
            ],
          ],
        ],
      ],
      // Check get introduction.
      [
        'configuration' => ['type' => 'introduction'],
        'source' => '<p>Introduction</p><h2>Question 1</h2><p>Answer 1</p><h2>Question 2</h2><p>Answer 2</p>',
        'expected_result' => '<p>Introduction</p>',
      ],
      // Check get questions.
      [
        'configuration' => ['type' => 'questions'],
        'source' => '<p>Introduction</p><h2>Question 1</h2><p>Answer 1</p><h2>Question 2</h2><p>Answer 2</p>',
        'expected_result' => [
          [
            'name' => 'Question 1',
            'accepted_answer' => '<p>Answer 1</p>',
          ],
          [
            'name' => 'Question 2',
            'accepted_answer' => '<p>Answer 2</p>',
          ],
        ],
      ],
    ];
  }

}
